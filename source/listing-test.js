// Composer

// Core
import React, { Component } from 'react';

// Instruments
import Styles from './styles.scss';
import { string, func } from 'prop-types';
import { getRandomColor } from '../../helpers';

export default class Composer extends Component {
    static contextTypes = {
        avatar:    string.isRequired,
        firstName: string.isRequired
    };

    static propTypes = {
        createPost: func.isRequired
    };

    constructor () {
        super();

        this.handleSubmit = ::this._handleSubmit;
        this.handleTextareaChange = ::this._handleTextareaChange;
        this.handleTextareaCopy = ::this._handleTextareaCopy;
        this.handleTextareaKeyPress = ::this._handleTextareaKeyPress;
        this.createPost = ::this._createPost;
    }

    state = {
        comment:           '',
        avatarBorderColor: '#90949C'
    };

    _handleSubmit (event) {
        event.preventDefault();
        this._createPost();
    }

    _createPost () {
        const { comment } = this.state;

        if (!comment) {
            return;
        }

        this.props.createPost({ comment });

        this.setState(() => ({
            comment: ''
        }));
    }

    _handleTextareaChange (event) {
        const { value: comment } = event.target;

        this.setState(() => ({ comment }));
    }

    _handleTextareaCopy (event) {
        event.preventDefault();
    }

    _handleTextareaKeyPress (event) {
        const enterKey = event.key === 'Enter';

        enterKey
            ? this.createPost()
            : this.setState(() => ({
                avatarBorderColor: getRandomColor()
            }));

        if (enterKey) {
            event.preventDefault();
        }
    }

    render () {
        const { avatar, firstName } = this.context;
        const { comment, avatarBorderColor } = this.state;

        return (
            <section className = { Styles.composer }>
                <img src = { avatar } style = { { borderColor: avatarBorderColor } } />
                <form onSubmit = { this.handleSubmit }>
                    <textarea
                        placeholder = { `What's on your mind, ${firstName}?` }
                        value = { comment }
                        onChange = { this.handleTextareaChange }
                        onCopy = { this.handleTextareaCopy }
                        onKeyPress = { this.handleTextareaKeyPress }
                    />
                    <input type = 'submit' value = 'Post' />
                </form>
            </section>
        );
    }
}

// Feed

// Core
import React, { Component } from 'react';

// Instruments
import Styles from './styles';
import { string } from 'prop-types';
import {
    Transition,
    CSSTransition,
    TransitionGroup
} from 'react-transition-group';
import { fromTo } from 'gsap';

// Components
import Composer from '../../components/Composer';
import Post from '../../components/Post';
import Catcher from '../../components/Catcher';
import Counter from '../../components/Counter';
import Spinner from '../../components/Spinner';
import Postman from '../../components/Postman';

export default class Feed extends Component {
    static contextTypes = {
        api:   string.isRequired,
        token: string.isRequired
    };

    constructor () {
        super();

        this.createPost = ::this._createPost;
        this.deletePost = ::this._deletePost;
        this.getPosts = ::this._getPosts;
        this.startPostsFetching = ::this._startPostsFetching;
        this.stopPostsFetching = ::this._stopPostsFetching;
        this.likePost = ::this._likePost;
        this.handleComposerAppear = ::this._handleComposerAppear;
        this.handleCounterAppear = ::this._handleCounterAppear;
        this.handlePostmanAppear = ::this._handlePostmanAppear;
        this.handlePostmanDisappear = ::this._handlePostmanDisappear;
    }

    state = {
        posts:         [],
        postsFetching: false
    };

    componentDidMount () {
        this.getPosts();

        this.refetch = setInterval(this.getPosts, 3000);
    }

    componentWillUnmount () {
        clearInterval(this.refetch);
    }

    _startPostsFetching () {
        this.setState(() => ({
            postsFetching: true
        }));
    }

    _stopPostsFetching () {
        this.setState(() => ({
            postsFetching: false
        }));
    }

    async _createPost ({ comment }) {
        try {
            const { api, token } = this.context;

            this.startPostsFetching();

            const response = await fetch(api, {
                method:  'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization:  token
                },
                body: JSON.stringify({
                    comment
                })
            });

            if (response.status !== 200) {
                this.stopPostsFetching();
                throw new Error('Post was not created!');
            }

            const { data } = await response.json();

            this.setState(({ posts }) => ({
                posts:         [data, ...posts],
                postsFetching: false
            }));
        } catch ({ message }) {
            console.log(message);
        }
    }

    async _getPosts () {
        try {
            const { api } = this.context;

            this.startPostsFetching();

            const response = await fetch(api, {
                method: 'GET'
            });

            if (response.status !== 200) {
                this.stopPostsFetching();
                throw new Error('Posts were not loaded.');
            }

            const { data: posts } = await response.json();

            this.setState(() => ({ posts, postsFetching: false }));
        } catch ({ message }) {
            console.log(message);
        }
    }

    async _deletePost (id) {
        try {
            const { api, token } = this.context;

            this.startPostsFetching();

            const response = await fetch(`${api}/${id}`, {
                method:  'DELETE',
                headers: {
                    Authorization: token
                }
            });

            if (response.status !== 204) {
                this.stopPostsFetching();
                throw new Error('Post was not deleted!');
            }

            this.setState(({ posts }) => ({
                posts:         posts.filter((post) => post.id !== id),
                postsFetching: false
            }));
        } catch ({ message }) {
            console.log(message);
        }
    }

    async _likePost (id) {
        try {
            const { api, token } = this.context;

            this.startPostsFetching();

            const response = await fetch(`${api}/${id}`, {
                method:  'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization:  token
                }
            });

            if (response.status !== 200) {
                this.stopPostsFetching();
                throw new Error('Post was not liked!');
            }

            const { data } = await response.json();

            this.setState(({ posts }) => ({
                posts: posts.map((post) => post.id === data.id ? data : post)
            }));

            this.stopPostsFetching();
        } catch ({ message }) {
            console.log(message);
        }
    }

    _handleComposerAppear (composer) {
        fromTo(
            composer,
            1,
            {
                y:         -200,
                x:         500,
                opacity:   0,
                rotationY: 360
            },
            {
                y:         0,
                x:         0,
                opacity:   1,
                rotationY: 0
            }
        );
    }

    _handleCounterAppear (counter) {
        fromTo(
            counter,
            1,
            {
                x:         -1000,
                y:         -300,
                opacity:   0,
                rotationY: 360
            },
            {
                x:         0,
                y:         0,
                opacity:   1,
                rotationY: 0
            }
        );
    }

    _handlePostmanAppear (postman) {
        fromTo(
            postman,
            2,
            {
                x:       500,
                opacity: 0
            },
            {
                x:       0,
                opacity: 1
            }
        );
    }

    _handlePostmanDisappear (postman) {
        fromTo(
            postman,
            2,
            {
                x:       0,
                opacity: 1
            },
            {
                x:       500,
                opacity: 0
            }
        );
    }

    render () {
        const { posts: postsData, postsFetching } = this.state;
        const posts = postsData.map(
            ({ avatar, comment, created, firstName, id, lastName, likes }) => (
                <CSSTransition
                    classNames = { {
                        enter:       Styles.postInStart,
                        enterActive: Styles.postInEnd,
                        exit:        Styles.postOutStart,
                        exitActive:  Styles.postOutEnd
                    } }
                    key = { id }
                    timeout = { { enter: 700, exit: 600 } }>
                    <Catcher>
                        <Post
                            avatar = { avatar }
                            comment = { comment }
                            created = { created }
                            deletePost = { this.deletePost }
                            firstName = { firstName }
                            id = { id }
                            lastName = { lastName }
                            likePost = { this.likePost }
                            likes = { likes }
                        />
                    </Catcher>
                </CSSTransition>
            )
        );

        const spinner = postsFetching ? <Spinner /> : null;

        return (
            <section className = { Styles.feed }>
                {spinner}
                <Transition
                    appear
                    in
                    timeout = { 1000 }
                    onEnter = { this.handleComposerAppear }>
                    <Composer createPost = { this.createPost } />
                </Transition>
                <Transition
                    appear
                    in
                    timeout = { 1000 }
                    onEnter = { this.handleCounterAppear }>
                    <Counter count = { posts.length } />
                </Transition>
                <TransitionGroup>{posts}</TransitionGroup>
                <Transition
                    appear
                    in
                    timeout = { 3000 }
                    onEnter = { this.handlePostmanAppear }
                    onEntered = { this.handlePostmanDisappear }>
                    <Postman />
                </Transition>
            </section>
        );
    }
}

//Like

// Core
import React, { Component } from 'react';

// Instruments
import Styles from './styles.scss';
import { string, func, arrayOf, shape } from 'prop-types';

export default class Like extends Component {
    static contextTypes = {
        firstName: string.isRequired,
        lastName:  string.isRequired
    };

    static propTypes = {
        id:       string.isRequired,
        likePost: func.isRequired,
        likes:    arrayOf(
            shape({
                firstName: string.isRequired,
                lastName:  string.isRequired
            }).isRequired
        ).isRequired
    };

    static defaultProps = {
        likes: []
    };

    constructor () {
        super();

        this.showLikers = ::this._showLikers;
        this.hideLikers = ::this._hideLikers;
        this.likePost = ::this._likePost;
        this.getLikedPosts = ::this._getLikedPosts;
        this.getLikersList = ::this._getLikersList;
        this.getTotalLikes = ::this._getTotalLikes;
    }

    state = {
        showLikers: false
    };

    _showLikers () {
        this.setState(() => ({
            showLikers: true
        }));
    }

    _hideLikers () {
        this.setState(() => ({
            showLikers: false
        }));
    }

    _likePost () {
        const { likePost, id } = this.props;
        const { firstName, lastName } = this.context;

        likePost(id, firstName, lastName);
    }

    _getLikedPosts () {
        const { firstName: ownFirstName, lastName: ownLastName } = this.context;

        return this.props.likes.some(
            ({ firstName, lastName }) =>
                `${firstName} ${lastName}` === `${ownFirstName} ${ownLastName}`
        );
    }

    _getLikersList () {
        const { likes } = this.props;
        const { showLikers } = this.state;

        return likes.length && showLikers ? (
            <ul>
                {likes.map(({ firstName, lastName, id }) => (
                    <li key = { id }>{`${firstName} ${lastName}`}</li>
                ))}
            </ul>
        ) : null;
    }

    _getTotalLikes () {
        const { likes } = this.props;
        const { firstName: ownFirstName, lastName: ownLastName } = this.context;

        const likedByMe = likes.some(
            ({ firstName, lastName }) =>
                `${firstName} ${lastName}` === `${ownFirstName} ${ownLastName}`
        );

        return likes.length === 1 && likedByMe
            ? `${ownFirstName} ${ownLastName}`
            : likes.length === 2 && likedByMe
                ? `You and ${likes.length - 1} other`
                : likedByMe ? `You and ${likes.length - 1} others` : likes.length;
    }

    render () {
        const likedPosts = this.getLikedPosts();
        const likeStyles = likedPosts
            ? `${Styles.icon} ${Styles.liked}`
            : `${Styles.icon}`;

        const likersList = this.getLikersList();
        const totalLikes = this.getTotalLikes();

        return (
            <section className = { Styles.like }>
                <span className = { likeStyles } onClick = { this.likePost }>
                    Like
                </span>
                <div>
                    {likersList}
                    <span
                        onMouseEnter = { this.showLikers }
                        onMouseLeave = { this.hideLikers }>
                        {totalLikes}
                    </span>
                </div>
            </section>
        );
    }
}

// Post

// Core
import React, { Component } from 'react';

// Instruments
import Styles from './styles';
import moment from 'moment';
import { string, func, number, arrayOf, shape } from 'prop-types';

// Components
import Like from '../../components/Like';

export default class Post extends Component {
    static contextTypes = {
        firstName: string.isRequired,
        lastName:  string.isRequired
    };

    static propTypes = {
        avatar:     string.isRequired,
        comment:    string.isRequired,
        created:    number.isRequired,
        deletePost: func.isRequired,
        firstName:  string.isRequired,
        id:         string.isRequired,
        lastName:   string.isRequired,
        likePost:   func.isRequired,
        likes:      arrayOf(
            shape({
                firstName: string.isRequired,
                lastName:  string.isRequired
            }).isRequired
        ).isRequired
    };

    constructor () {
        super();

        this.deletePost = ::this._deletePost;
    }

    shouldComponentUpdate (nextProps) {
        return JSON.stringify(nextProps) !== JSON.stringify(this.props);
    }

    _deletePost () {
        const { deletePost, id } = this.props;

        deletePost(id);
    }

    render () {
        const {
            avatar,
            comment,
            created,
            firstName,
            id,
            lastName,
            likes,
            likePost
        } = this.props;

        const { firstName: ownFirstName, lastName: ownLastName } = this.context;

        const isAbleToDelete =
            `${firstName} ${lastName}` === `${ownFirstName} ${ownLastName}` ? (
                <span className = { Styles.cross } onClick = { this.deletePost } />
            ) : null;

        return (
            <section className = { Styles.post }>
                {isAbleToDelete}
                <img src = { avatar } />
                <a>{`${firstName} ${lastName}`}</a>
                <time>{moment.unix(created).format('MMMM D h:mm:ss a')}</time>
                <p>{comment}</p>
                <Like id = { id } likePost = { likePost } likes = { likes } />
            </section>
        );
    }
}

// Spinner

// Core
import React from 'react';
import { createPortal } from 'react-dom';

// Instruments
import Styles from './styles';

const portal = document.getElementById('spinner');

const Spinner = () =>
    createPortal(<section className = { Styles.spinner } />, portal);

export default Spinner;

// Catcher

// Core
import React, { Component } from 'react';

// Instruments
import PropTypes from 'prop-types';
import Styles from './styles.scss';

export default class Catcher extends Component {
    static propTypes = {
        children: PropTypes.object.isRequired
    };

    state = {
        error: false
    };

    componentDidCatch (error, stack) {
        console.log('ERROR:', error.message);
        console.log('STACKTRACE:', stack.componentStack);

        this.setState(() => ({
            error: true
        }));
    }

    render () {
        const { error } = this.state;
        const { children } = this.props;

        if (error) {
            return (
                <section className = { Styles.catcher }>
                    <span>A mysterious 👽 &nbsp;error 📛 &nbsp;occured.</span>
                    <p>
                        Our space 🛰 &nbsp;engineers strike team 👩🏼‍🚀 👨🏼‍🚀
                        &nbsp;is already working 🚀 &nbsp;in order to fix that
                        for you!
                    </p>
                </section>
            );
        }

        return children;
    }
}

// Counter

// Core
import React from 'react';

// Instruments
import Styles from './styles.scss';
import { number } from 'prop-types';

const Counter = ({ count }) => (
    <section className = { Styles.counter }>Posts count: {count}</section>
);

Counter.propTypes = {
    count: number.isRequired
};

Counter.defaultProps = {
    count: 0
};

export default Counter;

// Postman

// Core
import React from 'react';

// Instruments
import Styles from './styles';
import PropTypes from 'prop-types';

const Postman = (props, { avatar, firstName }) => (
    <section className = { Styles.postman }>
        <img src = { avatar } />
        <span>Welcome online, {firstName}!</span>
    </section>
);

Postman.contextTypes = {
    avatar:    PropTypes.string.isRequired,
    firstName: PropTypes.string.isRequired
};

export default Postman;

//App

// Core
import React, { Component } from 'react';

// Instruments
import { string } from 'prop-types';
import avatar from '../../theme/assets/avatar.jpg';

// Components
import Feed from '../../components/Feed';
import Catcher from '../../components/Catcher';

const GROUP_ID = 'g9czxzzh2m';
const TOKEN = 'dh4qq08jv3';

const options = {
    api:       `https://lab.lectrum.io/react/api/${GROUP_ID}`,
    avatar,
    firstName: 'Oscar',
    lastName:  'Egillson',
    token:     TOKEN
};

export default class App extends Component {
    static childContextTypes = {
        api:       string.isRequired,
        avatar:    string.isRequired,
        firstName: string.isRequired,
        lastName:  string.isRequired,
        token:     string.isRequired
    };

    getChildContext () {
        return options;
    }

    render () {
        return (
            <Catcher>
                <Feed />
            </Catcher>
        );
    }
}


