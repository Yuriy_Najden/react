// Core
import React, { Component } from 'react';
import cx from 'classnames';
import { Transition } from 'react-transition-group';
import { fromTo } from 'gsap';

//Components
import { withProfile } from "components/HOC/withProfile";

//Instruments
import Styles from './styles.m.css';
import {socket} from "socket/index";

@withProfile
export default class StatusBar extends Component {
    state = {
      online: false,
    };

    componentDidMount () {
        socket.on('conect', () => {
           this.setState({
               online: true,
           });
        });

        socket.on('disconect', () => {
            this.setState({
                online: true,
            });
        });
    }

    componentWillUnmount () {
        socket.removeListener('conect');
        socket.removeListener('disconect');
    }

    _animateStatusBarEnter = (statusBar) => {
        fromTo(statusBar, 2,
            { opacity: 0, rotationX: 50,},
            { opacity: 1, rotationX: 0,}
        );
    };

    render(){

        const {
            avatar,
            currentUserFirstName,
            currentUserLastName,
        } = this.props;

        const { online } = this.state;

        const statusStyle = cx(Styles.status, {
            [Styles.online]: online,
            [Styles.offline]: !online,
        });

        const statusMessage = online ? 'Online' : 'Offline';

        return (
            <Transition
                in
                appear
                timeout = { 1000 }
                onEnter = { this._animateStatusBarEnter }>
                <section className = {Styles.statusBar}>
                    <div className = {statusStyle}>
                        <div>
                            {statusMessage}
                        </div>
                        <span />
                    </div>
                    <button>
                        <img src={avatar}/>
                        <span>{currentUserFirstName}</span>
                        &nbsp;
                        <span>{currentUserLastName}</span>
                    </button>
                </section>
            </Transition>
        );
    }
}