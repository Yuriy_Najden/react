// Core
import React from 'react';

// Instruments
import Styles from './styles';
import { withProfile } from 'components/HOC/withProfile';

const Postman = (props) => {
    return (
        <section className = { Styles.postman }>
            <img src = { props.avatar } />
            <span>Добро пожаловать online, { props.currentUserFirstName }!</span>
        </section>
    );
};

export default withProfile(Postman);
