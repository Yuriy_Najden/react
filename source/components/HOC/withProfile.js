//Core
import React, { Component, createContext } from 'react';

const { Provider, Consumer } = createContext();

const withProfile = (Enhancable) => {
    return class WithProfile extends Component {
        render(){
            return (
                <Consumer>
                    {(context) =>
                        <Enhancable
                            { ...context }
                            { ...this.props }
                        />
                    }
                </Consumer>
            );
        }
    };
};

export { Provider, Consumer, withProfile };